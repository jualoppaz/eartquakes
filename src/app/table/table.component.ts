import { Component, OnInit } from '@angular/core';

import { DaterangepickerConfig } from 'ng2-daterangepicker';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor(private daterangepickerOptions: DaterangepickerConfig) {
    this.daterangepickerOptions.skipCSS = true;
  }

  public daterange: any = {};

    // see original project for full list of options
    // can also be setup using the config service to apply to multiple pickers
    public options: any = {
        locale: { format: 'DD-MM-YYYY' },
        alwaysShowCalendars: false,
    };

    public selectedDate(value: any) {
        // this is the date the iser selected
        console.log("Entramos en el selectedDate");
        console.log(value);

        // or manupulat your own internal property
        this.daterange.start = value.start;
        this.daterange.end = value.end;
        this.daterange.label = value.label;
    }

    public calendarCanceled(e:any) {
      console.log(e);
      // e.event
      // e.picker
    }

    public calendarApplied(e:any) {
        console.log(e);
        // e.event
        // e.picker
    }

  ngOnInit() {
    console.log("Fecha de inicio: " + this.daterange.start);
  }

}