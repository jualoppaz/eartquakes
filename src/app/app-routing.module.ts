import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { AuthGuard } from './auth/auth.guard';

// Components
import { LoginComponent } from './login/login.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { TableComponent } from './table/table.component';
import { MapComponent } from './map/map.component';

const routes: Routes = [
    { 
      path: 'login', 
      component: LoginComponent,
      data: {
        code: 'login'
      } 
    },{
      path: 'table',                       
      component: HomeLayoutComponent,
      canActivate: [AuthGuard],      
      children: [
        {
          path: '',
          component: TableComponent,
          data: {
            code: 'table'
          }
        }
      ]
    },{
      path: 'map',
      component: HomeLayoutComponent,
      canActivate: [AuthGuard],
      children: [
        {
          path: '',
          component: MapComponent,
          data: {
            code: 'map'
          }
        }
      ]
    },{ 
      path: '**',
      redirectTo: '/login',
      pathMatch: 'full'
    }
  ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}