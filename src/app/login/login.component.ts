import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../auth/auth.service';

import { ToastrService } from 'ngx-toastr';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  form: FormGroup;                    
  private formSubmitAttempt: boolean;

  constructor(
    private fb: FormBuilder, 
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.form = this.fb.group({     
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    if(this.authService.isLoggedIn){
      console.log("Redirigimos a la pantalla inicial");
      this.router.navigate(['/table']);
    }
  }

  onSubmit() {
    if (this.form.valid) {
      this.authService.login(this.form.value);

      if(!this.authService.isLoggedIn.value){
        this.showError();
      }
    }else{
      this.showError();
    }
    this.formSubmitAttempt = true;             
  }

  showError() {
    this.toastr.error('Bad credentials!', 'Authentication error');
  }
}
