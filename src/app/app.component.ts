import { Component } from '@angular/core';
import { Location } from '@angular/common';

import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { UrlSegment } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  route: string;
  
  constructor(private router: Router, private activatedRoute: ActivatedRoute){

  }

  ngOnInit(){
    this.router.events
      .filter(e => e instanceof NavigationEnd)
      .forEach(e => {
        this.route = this.activatedRoute.root.firstChild.snapshot.data['code'];

        /*if(this.route === ''){
          this.router.navigate(['/table']);
        }*/
    });
  }
}
