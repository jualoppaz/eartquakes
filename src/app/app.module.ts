import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppBootstrapModule } from './app-bootstrap/app-bootstrap.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

import { AppRoutingModule } from './app-routing.module';
import { TableComponent } from './table/table.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { HeaderComponent } from './header/header.component';

import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MapComponent } from './map/map.component';

import { Daterangepicker } from 'ng2-daterangepicker';

@NgModule({
  declarations: [AppComponent, LoginComponent, TableComponent, HomeLayoutComponent, HeaderComponent, MapComponent],
  imports: [
    BrowserModule, 
    AppBootstrapModule, 
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    Daterangepicker
  ],

  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}