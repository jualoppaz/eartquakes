import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from './user';

@Injectable()
export class AuthService {
  private key: string = 'user';
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(localStorage.getItem(this.key) === '1');

  get isLoggedIn() {
    return this.loggedIn;
  }

  constructor(private router: Router) {

  }

  login(user: User){
    console.log("Credenciales recibidas: " + user.username + " / " + user.password);
    if (user.username === 'admin' && user.password === '12345' ){
      localStorage.setItem(this.key, '1');
      this.loggedIn.next(true);
      this.router.navigate(['/table']);
    }
  }

  logout(){
    localStorage.removeItem(this.key);
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }
}