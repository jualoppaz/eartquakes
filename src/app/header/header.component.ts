import { Component, OnInit } from '@angular/core';

import { AuthService } from './../auth/auth.service';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private toastr: ToastrService
  ) { }

  isCollapsed: boolean = true;

  ngOnInit() {

  }

  logout(){
    this.authService.logout();

    if(!this.authService.isLoggedIn.value){
      this.showInfo();
    }
  }

  showInfo(){
    this.toastr.info('Hope see you soon!', 'Bye');
  }
}