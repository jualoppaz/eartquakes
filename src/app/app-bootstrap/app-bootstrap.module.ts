import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { 
  BsDropdownModule,
  TooltipModule,
  ModalModule,
  CollapseModule
 } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot()
  ],
  exports: [BsDropdownModule, TooltipModule, ModalModule, CollapseModule]
})
export class AppBootstrapModule {}